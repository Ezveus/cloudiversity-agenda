class Agenda::Assignment < ActiveRecord::Base
    belongs_to :teacher_school_class_discipline
    has_many :progresses, dependent: :destroy

    include Attachable

    validates_presence_of :title, :deadline, :wording, :teacher_school_class_discipline

    delegate :school_class, to: :teacher_school_class_discipline
    delegate :teacher, to: :teacher_school_class_discipline
    delegate :discipline, to: :teacher_school_class_discipline

    alias_method :tscd, :teacher_school_class_discipline

    # Fetches the progress for a given student.
    # If the progress doesn't exist, report it as 0.
    def progress_for(student)
        progress = progresses.find_by student: student
        progress ? progress.progress : 0
    end
end

::TeacherSchoolClassDiscipline.send(:has_many, :agenda_assignments, { class_name: 'Agenda::Assignment' })
