class Agenda::Progress < ActiveRecord::Base
    belongs_to :student
    belongs_to :assignment

    validates_presence_of :progress, :student, :assignment
    validates_inclusion_of :progress, { in: 0..100, message: "should be included between 0 and 100%." }

    delegate :school_class, to: :assignment
    delegate :teacher, to: :assignment
    delegate :discipline, to: :assignment
end

::Student.send(:has_many, :agenda_progresses, { class_name: 'Agenda::Progress' })
