class Agenda::AssignmentsController < ApplicationController
    def index
        # Required as it is not automatically loaded, so we call
        # it to make Rails load it.
        # A better solution is needed...
        Agenda::Assignment

        if current_user.roles.count > 1
            index_multirole
        elsif current_user.is_student?
            index_student
        elsif current_user.is_teacher?
            index_teacher
        elsif current_user.is_admin?
            index_admin
        elsif current_user.is_parent?
            index_parent
        else
            raise Pundit::NotAuthorizedError
        end
    end

    def new
        @agenda_assignment = Agenda::Assignment.new
        authorize(@agenda_assignment)
    end

    def create
        respond_to do |format|
            format.html do
                @agenda_assignment = Agenda::Assignment.new(permitted_params)
                authorize(@agenda_assignment)

                if @agenda_assignment.save
                    redirect_to @agenda_assignment, notice: 'Assignment succesfully created'
                else
                    render action: :new
                end
            end
            format.json do
                if current_user.is_teacher?
                    data = create_teacher_json
                    if data[:error]
                        render json: data, status: :internal_server_error
                    else
                        render json: data
                    end
                else
                    authorize(Agenda::Assignment)

                    render json: { error: "You are not allowed to access this data." }, status: :forbidden
                end
            end
        end
    end

    def edit
        @agenda_assignment = Agenda::Assignment.find(params[:id])
        authorize(@agenda_assignment)
    end

    def update
        @agenda_assignment = Agenda::Assignment.find(params[:id])
        authorize(@agenda_assignment)

        respond_to do |format|
            format.html do
                if request.referer == agenda_assignment_url(@agenda_assignment) &&
                    current_user.is_student? &&
                    current_user.as_student.teacher_school_class_disciplines.map { |tscd| tscd.agenda_assignments.include? @agenda_assignment }.include?(true)
                    p = @agenda_assignment.progresses.find_by(student: current_user.as_student)
                    if p.nil?
                        p = Agenda::Progress.new(student: current_user.as_student,
                                                   assignment: @agenda_assignment,
                                                   progress: params[:agenda_assignment][:progress])
                        if p.save
                            redirect_to @agenda_assignment, notice: 'Progression succesfully updated'
                        else
                            redirect_to @agenda_assignment, notice: p.errors.full_messages.join("\n")
                        end
                    elsif p.update_attributes(progress: params[:agenda_assignment][:progress])
                        redirect_to @agenda_assignment, notice: 'Progression succesfully updated'
                    else
                        redirect_to @agenda_assignment, notice: p.errors.full_messages.join("\n")
                    end
                elsif @agenda_assignment.update(permitted_params)
                    redirect_to @agenda_assignment, notice: 'Assignment succesfully updated'
                else
                    render action: :edit
                end
            end
            format.json do
                if current_user.roles.count > 1
                    if params[:as].nil?
                        render json: { error: "You have multiple roles. Use a 'as' parameter to precise type of response." }, status: :bad_request
                    elsif !current_user.roles.map { |role| role.role_type.downcase }.include?(params[:as])
                        render json: { error: "You do not have that role." }, status: :forbidden
                    else
                        data = send("update_#{params[:as]}_json")
                        if data[:error]
                            render json: data, status: :internal_server_error
                        else
                            render json: data
                        end
                    end
                elsif current_user.is_student?
                    data = update_student_json
                    if data[:error]
                        render json: data, status: :internal_server_error
                    else
                        render json: data
                    end
                elsif current_user.is_teacher?
                    data = update_teacher_json
                    if data[:error]
                        render json: data, status: :internal_server_error
                    else
                        render json: data
                    end
                else
                    render json: { error: "You are not allowed to access this data." }, status: :forbidden
                end
            end
        end
    end

    def show
        @agenda_assignment = Agenda::Assignment.find(params[:id])
        authorize(@agenda_assignment)

        respond_to do |format|
            format.html {}
            format.json do
                if current_user.roles.count > 1
                    if params[:as].nil?
                        render json: { error: "You have multiple roles. Use a 'as' parameter to precise type of response." }, status: :bad_request
                    elsif !current_user.roles.map { |role| role.role_type.downcase }.include?(params[:as])
                        render json: { error: "You do not have that role." }, status: :forbidden
                    else
                        data = send("show_#{params[:as]}_json")
                        if data[:error]
                            render json: data, status: :internal_server_error
                        else
                            render json: data
                        end
                    end
                elsif current_user.is_teacher?
                    data = show_teacher_json
                    if data[:error]
                        render json: data, status: :internal_server_error
                    else
                        render json: data
                    end
                elsif current_user.is_student?
                    data = show_student_json
                    if data[:error]
                        render json: data, status: :internal_server_error
                    else
                        render json: data
                    end
                else
                    render json: { error: "You are not allowed to access this data." }, status: :forbidden
                end
            end
        end
    end

    def destroy
        @agenda_assignment = Agenda::Assignment.find(params[:id])
        authorize(@agenda_assignment)
        @agenda_assignment.destroy
        redirect_to agenda_root_path
    end

    def destroy_confirmation
        @agenda_assignment = Agenda::Assignment.find(params[:id])
        authorize(@agenda_assignment)
    end

    def index_per_tscd
        teacher_id = params[:teacher_id] || (current_user.as_teacher.id if current_user.is_teacher?)

        tscd = TeacherSchoolClassDiscipline.find_by(teacher_id: teacher_id,
                                                       discipline_id: params[:discipline_id],
                                                       school_class_id: params[:school_class_id])
        authorize(tscd)

        # Required as it is not automatically loaded, so we call
        # it to make Rails load it.
        # A better solution is needed...
        Agenda::Assignment

        respond_to do |format|
            format.html { @agenda_assignments = tscd.agenda_assignments.group_by { |e| e.deadline } }
            format.json {
                data = tscd.agenda_assignments.map do |e|
                    {
                        id: e.id,
                        title: e.title,
                        deadline: e.deadline,
                        duetime: e.duetime ? e.duetime.utc.to_s(:time) : nil,
                        discipline: {
                            id: e.discipline.id,
                            name: e.discipline.name
                        },
                        school_class: {
                            id: e.school_class.id,
                            name: e.school_class.name
                        }
                    }
                end
                render json: data
            }
        end
    end

    # This method allows to raise a better exception for a missing role method
    def method_missing(m, *args, &block)
        if /index_(?<role_name>\w+)/ =~ m.to_s ||
            /get_(?<role_name>\w+)_data/ =~ m.to_s ||
            /update_(?<role_name>\w+)_json/ =~ m.to_s
            if request.headers['Content-Type'] == "application/json"
                return { error: "The role #{role_name} doesn't exist or isn't managed by this controller" }
            else
                raise "The role #{role_name} doesn't exist or isn't managed by this controller"
            end
        else
            super
        end
    end

    private
    def permitted_params(key = :agenda_assignment)
        params.require(key).permit(:wording,
                                   :title,
                                   :deadline,
                                   :duetime,
                                   :teacher_school_class_discipline_id,
                                   attachments_attributes: Attachment::PermittedParams
                                   )
    end

    # Get the raw data for a student
    def get_student_data(data_mapping = nil)
        data = policy_scope(TeacherSchoolClassDiscipline)
        data = data["student"] if current_user.roles.count > 1
        data = data.all.map { |e| e.agenda_assignments }
        data.flatten!

        if data_mapping == :html
            data.group_by { |e| e.deadline }
            # Tri par heure, tri par duetime, regroupement des progress == 100%
            # Si date = demain, show demain
        elsif data_mapping == :json
            data.map! do |a|
                progress = a.progresses.find_by(student: current_user.as_student)
                progress = progress ? progress.progress : 0

                {
                    id: a.id,
                    title: a.title,
                    deadline: a.deadline,
                    duetime: a.duetime.nil? ? nil : a.duetime.utc.to_s(:time),
                    progress: progress,
                    discipline: {
                        id: a.discipline.id,
                        name: a.discipline.name
                    }
                }

            end
        end
    end

    # Generate the student data for the index action
    def index_student
        respond_to do |f|
            f.json {
                render json: get_student_data(:json)
            }
            f.html {
                @data = get_student_data(:html)
                render 'index_student'
            }
        end
    end

    # Get the raw data for a teacher
    def get_teacher_data(data_mapping = nil)
        data = policy_scope(TeacherSchoolClassDiscipline)
        data = data["teacher"] if current_user.roles.count > 1
        data = data.all.group_by { |e| e.discipline_id }.map do |_, tscd|
            {
                id: tscd.first.discipline.id,
                name: tscd.first.discipline.name,
                school_classes: tscd.group_by { |e| e.school_class_id }.map do |_, tscd|
                    {
                        id: tscd.first.school_class.id,
                        name: tscd.first.school_class.name,
                        assignment_count: tscd.first.agenda_assignments.count
                    }
                end
            }
        end

        if data_mapping == :html
            data.map! do |discipline|
                discipline[:school_classes].map! do |cls|
                    OpenStruct.new(cls)
                end

                OpenStruct.new(discipline)
            end
        elsif data_mapping == :json
            data
        end
    end

    # Generate the teacher data for the index action
    def index_teacher
        respond_to do |f|
            f.json { render json: get_teacher_data(:json) }
            f.html {
                @data = get_teacher_data(:html)
                render 'index_teacher'
            }
        end
    end

    # Get the raw data for an admin
    def get_admin_data(data_mapping = nil)
        data = policy_scope(TeacherSchoolClassDiscipline)
        data = data["admin"] if current_user.roles.count > 1

        data = data.all.group_by { |e| e.teacher_id }.map do |_, tscd|
            {
                id: tscd.first.teacher.id,
                full_name: tscd.first.teacher.full_name,
                disciplines: tscd.group_by { |e| e.discipline_id }.map do |_, tscd|
                    {
                        id: tscd.first.discipline.id,
                        name: tscd.first.discipline.name,
                        school_classes: tscd.group_by { |e| e.school_class_id }.map do |_, tscd|
                            {
                                id: tscd.first.school_class.id,
                                name: tscd.first.school_class.name,
                                assignment_count: tscd.first.agenda_assignments.count
                            }
                        end
                    }
                end
            }
        end

        if data_mapping == :html
            data.map! do |teacher|
                teacher[:disciplines].map! do |discipline|
                    discipline[:school_classes].map! do |cls|
                        OpenStruct.new(cls)
                    end

                    OpenStruct.new(discipline)
                end
                OpenStruct.new(teacher)
            end
        elsif data_mapping == :json
            data
        end
    end

    # Generate the admin data for the index action
    def index_admin
        respond_to do |f|
            f.json { render json: get_admin_data(:json) }
            f.html {
                @data = get_admin_data(:html)
                render 'index_admin'
            }
        end
    end

    # Get the raw data for a parent
    def get_parent_data(data_mapping = nil)
        # TODO: get_parent_data
    end

    # Generate the parent data for the index action
    def index_parent
        # TODO: index_data
    end

    # Manages the index action rendering in case of multiple roles
    def index_multirole
        respond_to do |format|
            format.html do
                @data = Hash[current_user.roles.map { |role| [role.role_type.downcase,
                    send("get_#{role.role_type.downcase}_data", :html)] }]
            end
            format.json do
                policy_scope(Agenda::Assignment)
                if params[:as].nil?
                    render json: { error: "You have multiple roles. Use a 'as' parameter to precise type of response." }, status: :bad_request
                elsif !current_user.roles.map { |role| role.role_type.downcase }.include?(params[:as])
                    render json: { error: "You do not have that role." }, status: :forbidden
                else
                    render json: send("get_#{params[:as]}_data", :json)
                end
            end
        end
    end

    def update_student_json
        p = @agenda_assignment.progresses.find_by(student: current_user.as_student)
        progress_int = (params[:assignment][:progress] if params[:assignment]) || nil
        if p.nil?
            p = Agenda::Progress.new(student: current_user.as_student,
                                       assignment: @agenda_assignment,
                                       progress: progress_int)
            if p.save
                { id: @agenda_assignment.id, progress: p.progress }
            else
                { error: p.errors.full_messages }
            end
        elsif p.update_attributes(progress: progress_int)
            { id: @agenda_assignment.id, progress: p.progress }
        else
            { error: p.errors.full_messages }
        end
    end

    def update_teacher_json
        discipline_id = params[:assignment][:discipline_id] || @agenda_assignment.discipline.id
        school_class_id = params[:assignment][:school_class_id] || @agenda_assignment.school_class.id
        tscd = TeacherSchoolClassDiscipline.find_by(teacher_id: current_user.as_teacher.id,
                                                    discipline_id: discipline_id,
                                                    school_class_id: school_class_id)
        if tscd.nil?
            return { error: "This teacher doesn't own this discipline or this school class." }
        else
            params[:assignment][:teacher_school_class_discipline_id] = tscd.id
        end
        if @agenda_assignment.update_attributes(permitted_params(:assignment))
            show_teacher_json
        else
            { error: @agenda_assignment.errors.full_messages }
        end
    end

    def show_student_json
        if current_user.as_student.teacher_school_class_disciplines.map { |tscd| tscd.agenda_assignments.include? @agenda_assignment }.include?(true)
            {
                id: @agenda_assignment.id,
                title: @agenda_assignment.title,
                deadline: @agenda_assignment.deadline,
                duetime: @agenda_assignment.duetime ? @agenda_assignment.duetime.utc.to_s(:time) : nil,
                wording: @agenda_assignment.wording,
                created_at: @agenda_assignment.created_at.utc.to_s(:db),
                updated_at: @agenda_assignment.updated_at.utc.to_s(:db),
                progress: @agenda_assignment.progress_for(current_user.as_student)
            }
        else
            { error: "You are not allowed to access this data." }
        end
    end

    def show_teacher_json
        if current_user.as_teacher.tscd.map { |tscd| tscd.agenda_assignments.include? @agenda_assignment }.include?(true)
            {
                id: @agenda_assignment.id,
                title: @agenda_assignment.title,
                deadline: @agenda_assignment.deadline,
                duetime: @agenda_assignment.duetime ? @agenda_assignment.duetime.utc.to_s(:time) : nil,
                wording: @agenda_assignment.wording,
                created_at: @agenda_assignment.created_at.utc.to_s(:db),
                updated_at: @agenda_assignment.updated_at.utc.to_s(:db),
                discipline: {
                    id: @agenda_assignment.discipline.id,
                    name: @agenda_assignment.discipline.name
                },
                school_class: {
                    id: @agenda_assignment.school_class.id,
                    name: @agenda_assignment.school_class.name
                }
            }
        else
            { error: "You are not allowed to access this data." }
        end
    end

    def create_teacher_json
        discipline_id = params[:assignment][:discipline_id]
        school_class_id = params[:assignment][:school_class_id]
        tscd = TeacherSchoolClassDiscipline.find_by(teacher_id: current_user.as_teacher.id,
                                                    discipline_id: discipline_id,
                                                    school_class_id: school_class_id)
        if tscd.nil?
            authorize(Agenda::Assignment)
            return { error: "This teacher doesn't own this discipline or this school class." }
        else
            params[:assignment][:teacher_school_class_discipline_id] = tscd.id
        end
        @agenda_assignment = Agenda::Assignment.new(permitted_params(:assignment))
        authorize(@agenda_assignment)
        if @agenda_assignment.save
            show_teacher_json
        else
            { error: @agenda_assignment.errors.full_messages }
        end
    end
end
