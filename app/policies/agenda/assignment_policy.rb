class Agenda::AssignmentPolicy < ApplicationPolicy
    class Scope < Struct.new(:user, :scope)
        def resolve
            scope
        end
    end

    def index?
        true
    end

    def create?
        user.is_admin? || user.is_teacher?
    end

    def new?
        create?
    end

    def update?
        user.is_admin? || user_can_update?
    end

    def edit?
        user.is_admin? || user_can_edit_or_destroy?
    end

    def destroy?
        edit?
    end

    def destroy_confirmation?
        destroy?
    end

    def show?
        !(record.nil? && user.roles.empty?) && user_can_see?
    end

    private
    ## Returns true if the current user can see the record
    def user_can_see?
        return true if user.is_admin?
        (user.as_teacher.tscd.map { |tscd| tscd.agenda_assignments.include? record }.include?(true) if user.is_teacher?) ||
        (user.as_student.teacher_school_class_disciplines.map { |tscd| tscd.agenda_assignments.include? record }.include?(true)  if user.is_student?)
        # TODO: Manage parent
    end

    ## Returns true if the current user can edit/destroy the record
    def user_can_edit_or_destroy?
        (user.is_teacher? && user.as_teacher.tscd.map { |tscd| tscd.agenda_assignments.include? record }.include?(true))
    end

    ## Returns true if the current user can update the record
    def user_can_update?
        user_can_edit_or_destroy? ||
        (user.is_student? && user.as_student.teacher_school_class_disciplines.map { |tscd| tscd.agenda_assignments.include? record }.include?(true))
    end
end
