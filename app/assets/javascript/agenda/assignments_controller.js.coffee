$ ->
    $('#add_duetime').change(-> $('#cl-agenda_assignment-duetime-row').toggleClass("uk-hidden")) # On #new and #edit

    $('#progress-slider').slider({
        step: 10,
        value: $('#agenda_assignment_progress').val(),
        change: (event, ui) ->
            $('#agenda_assignment_progress').val(ui.value)
            $('#progress-submission').removeAttr("disabled")
    }) # On #show
