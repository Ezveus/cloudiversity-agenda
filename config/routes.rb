Rails.application.routes.draw do
    namespace :agenda do
        root 'assignments#index'
        resources :assignments do
            member do
                get :destroy_confirmation
            end
        end

        get '/assignments/:teacher_id/:discipline_id/:school_class_id', to: 'assignments#index_per_tscd'
        get '/assignments/:teacher_id/:discipline_id/:school_class_id', to: 'assignments#index_per_tscd'
        get '/assignments/:discipline_id/:school_class_id', to: 'assignments#index_per_tscd'
        post '/assignments/:id', to: 'assignments#update'
    end
end
