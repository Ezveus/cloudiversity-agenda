# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :agenda_assignment, :class => 'Agenda::Assignment' do
    title "MyString"
    deadline "2014-04-23"
    wording "MyText"
    teacher_school_class_discipline nil
  end
end
