# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
    factory :agenda_progress, :class => 'Agenda::Progress' do
        students nil
        agenda_assignments nil
        progress 1
    end
end
