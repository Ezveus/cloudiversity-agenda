$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "agenda/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
    s.name        = "cloudiversity_agenda"
    s.version     = Agenda::VERSION
    s.authors     = ["Jean-Baptiste Brenaut", "Matthieu Ciappara"]
    s.email       = ["jeanbaptiste.brenaut@gmail.com", "ciappam@gmail.com"]
    s.homepage    = "http://cloudiversity.eu"
    s.summary     = "The agenda module for Cloudiversity."
    s.description = "The agenda module for Cloudiversity."
    s.license     = "MIT"

    s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.rdoc"]

    s.add_dependency "rails", "~> 4.0.4"
    s.add_dependency "haml-rails"
    s.add_dependency "pundit"

    s.add_development_dependency "sqlite3"
    s.add_development_dependency "rspec-rails"
    s.add_development_dependency "factory_girl_rails"

    s.test_files = Dir["spec/**/*"]
end
