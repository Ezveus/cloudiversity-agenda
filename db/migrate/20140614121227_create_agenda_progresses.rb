class CreateAgendaProgresses < ActiveRecord::Migration
    def change
        create_table :agenda_progresses do |t|
            t.references :student, index: true
            t.references :assignment, index: true
            t.integer :progress

            t.timestamps
        end
    end
end
