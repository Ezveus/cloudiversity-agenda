class CreateAgendaAssignments < ActiveRecord::Migration
  def change
    create_table :agenda_assignments do |t|
      t.string :title
      t.date :deadline
      t.text :wording
      t.belongs_to :teacher_school_class_discipline, index: true
      t.time :duetime

      t.timestamps
    end
  end
end
